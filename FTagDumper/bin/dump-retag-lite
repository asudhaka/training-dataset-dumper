#!/usr/bin/env python

"""Lightweight retagging script

Runs some variations and saves the GN2 scores on jets.

This is distinct from the "full" tagging that we use in most flavor
tagging. You can run that with `dump-retag`.

The main differences here are:

- Athena is optional (required for the "rich"  track extrapolation)
- No b-tagging object is created
- No secondary vertexing code is run, this is strictly NNs

"""

from argparse import ArgumentParser
import sys

from AthenaConfiguration.AllConfigFlags import initConfigFlags
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.MainServicesConfig import MainServicesCfg as getConfig
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

from FTagDumper import dumper
from FTagDumper import retag_lite as retag


def get_args():
    """
    Extend the base dumper argument parser.
    """
    parser = ArgumentParser(
        formatter_class=dumper.DumperHelpFormatter,
        parents=[dumper.base_parser(__doc__, add_help=False)],
    )
    return parser.parse_args()


def run():

    args = get_args()

    flags = initConfigFlags()
    dumper.update_cfgFlags(flags, args)
    flags.lock()

    ca = getConfig(flags)
    ca.merge(PoolReadCfg(flags))
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))

    # TODO: get the jet and track collection out of this file, rather
    # than relying on arguments from the user
    config = dumper.combinedConfig(args.config_file)
    jet_collection = config['dumper']['jet_collection']


    # add lightweight b-tagging
    #
    # TODO: also specify the name of the output tracks here, to ensure
    # that we can schedule several track semaring variations in
    # parallel too
    systs = dict(
        track_systs=config['track_systematics'],
        flip_configs=[retag.FlipConfig[x] for x in config['flip_configs']],
        jet_systs=config['jet_systematics'],
        jet_sigma=config['jet_sigma'],
        ip_method=retag.IPPrefix[config['ip_method']],
    )

    # Throw an error if both track and jet systematics 
    # are applied simultaneously since you can only vary one kind at a time.
    if (len(systs["track_systs"])>0 and len(systs["jet_systs"])>0):
        raise Exception("ERROR: One systematic type (jet or track) at a time!")
    
    ca.merge(
        retag.retagging(
            flags,
            track_collection=config['track_container'],
            decorate_track_truth=config['decorate_track_truth'],
            jet_collection=jet_collection,
            add_fold_hash=config['add_fold_hash'],
            **systs,
        )
    )

    # add dumper
    ca.merge(
        dumper.getDumperConfig(
            args,
            config_dict=retag.mungedConfig(config['dumper'], **systs)
        )
    )

    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
