#!/usr/bin/env bash

set -eu

# First argument is number of events, any remaining are containers to select from
NUM_EVENTS=${1:--1}
shift
INPUT_DATASETS=("$@")


if ! [[ $NUM_EVENTS =~ ^-?[0-9]+$ ]]; then
    echo "'-n' argument must be an integer, not '$NUM_EVENTS'"  >&2
    exit 1
fi


# Create an array to store all final files to submit
declare -A CONTAINER_TO_FILES


echo "Loading $NUM_EVENTS events from input datasets" >&2



# Iterate each container, get all files in container, 
# iterate through files, storing the cumulative sum of events in files until
# number of events requested is reached.
    for container in "${INPUT_DATASETS[@]}"; do
    containter_files=""
    num_files=-1
    declare -a files
    while IFS= read -r line; do
        files+=("$line")
    done < <(rucio list-files $container)

    total_files=${files[-3]##*:}
    total_events=${files[-1]##*:}

    if [ "$NUM_EVENTS" -gt "$total_events" ]; then
        # If we request more events than are available, then we load all events by only selecting the full
        # container name
        echo "Requested $NUM_EVENTS events, but only $total_events events available in container $container" >&2
        # FILES_TO_SUBMIT+=($container)
        containter_files="$container "
        CONTAINER_TO_FILES[$container]=-1
        continue
    else
        echo "Requested $NUM_EVENTS events, $total_events available in container $container" >&2
        num_events=$NUM_EVENTS
    fi

    num_collected_events=0
    # Skip the first 3 lines as they are table headers/lines
    for ((i=3; i<total_files+3; i++)); do
        # Get the file
        line="${files[i]}"
        # Extract the number of events in the file, and its name
        event_value=$(awk -F '|' '{ num += $6 } END { printf "%d", int(num) }' <<< "$line")
        fname=$(echo $line | awk -F '|' '{print $2}')
        containter_files+="$fname "
        num_collected_events=$((num_collected_events + event_value))

        if [ $num_collected_events -ge $num_events ]; then
            echo "For container " $container ", will submit " $((i-2)) " files to grid for " $num_collected_events " total events" >&2
            num_files=$((i-2))
            break
        fi
    done

    unset files
    CONTAINER_TO_FILES[$container]=$num_files
done

for key in "${!CONTAINER_TO_FILES[@]}"; do
    echo "$key:${CONTAINER_TO_FILES[$key]}"
done