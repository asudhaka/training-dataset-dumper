#ifndef DL2_CONFIG_HH
#define DL2_CONFIG_HH

#include <string>
#include <vector>
#include <map>
#include <set>

#include "FlavorTagDiscriminants/FlipTagEnums.h"


struct DL2Config {
  enum class Where {UNKNOWN, JET, BTAG};
  Where where;
  enum class Engine {UNKNOWN, DL2, GNN, XBB};
  Engine engine;
  std::string nn_file_path;
  std::map<std::string, std::string> remapping;
  FlavorTagDiscriminants::FlipTagConfig flip_tag_config;
  bool decorate_tracks;
};

#endif
