#ifndef PROCESS_SINGLE_BTAG_EVENT_HH
#define PROCESS_SINGLE_BTAG_EVENT_HH

class JetDumperConfig;
class JetDumperTools;
class JetDumperOutputs;
namespace xAOD {
  class TEvent;
}

// the joy of "duel use": there doesn't appear to be a common
// baseclass that implements the one method we need in TEvent and
// StoreGate (i.e. `retrieve`). So we use overloads here, templates
// are defined in processEvent.tcc

void processEvent(xAOD::TEvent&,
                            const JetDumperConfig&,
                            const JetDumperTools&,
                            JetDumperOutputs&);

// we only build this guy if we're using a Gaudi build
#ifndef XAOD_STANDALONE
class StoreGateSvc;
void processEvent(StoreGateSvc&,
                            const JetDumperConfig&,
                            const JetDumperTools&,
                            JetDumperOutputs&);
#else
// for the non-Gaudi build, there's SgTEvent, which is some kind of
// wrapper for TEvent that looks (kind of) like StoreGate
namespace asg {
  class SgTEvent;
}
void processEvent(asg::SgTEvent&,
                            const JetDumperConfig&,
                            const JetDumperTools&,
                            JetDumperOutputs&);
#endif  // XAOD_STANDALONE

#endif  // PROCESS_SINGLE_BTAG_EVENT_HH
